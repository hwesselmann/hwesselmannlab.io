---
layout: post
title: Manchmal ist es ok, nichts zu tun
tags:
- agile
- scrum
- xp
- aktionismus
- teams
---

Als Scrum Master wird man darauf indoktriniert, sein Team zu kontinuierlicher Verbesserung zu treiben und Hindernisse aus dem Weg zu räumen.
Anfangs habe ich dabei gedacht: "Ja klar, ich will ja, dass alle immer besser werden, mehr Stories, mehr Qualität, mehr Wert für die Geldgeber.
Das geht natürlich nur, wenn ich meinem Methodenkoffer immer mit frischen Inhalten fülle und immer zu Veränderungen motiviere."

Daraus folgte eine Übersensibilisierung für Unwichtiges und das stetige Nerven meine täglichen Umfeldes. Aus Problemchen wurde große Schwierigkeiten, ein Diskussion
bedeutete manchmal das drohende Auseinanderbrechen des Teams. Eine Idee eines Teammitglieds wurde zu dringendem Handlungsbedarf und der neueste Trend aus dem
coolen Podcast oder dem interessanten Blogposting musste mit der Veränderungsbrechstange ins Team gebracht werden.

Zu dumm, dass man mit diesem Vorgehen sehr schnell gar nichts mehr erreicht, und als dogmatischer, aktionistischer Agil-Heini abgestempelt wird!
Rückblickend bin ich dadurch vom Scrum Master zum Scrum Manager geworden (was wahrscheinlich kein guter Karriereschritt ist). Aber: Eine Rücktransformation war zum Glück möglich!

Mittlerweile weiß ich, dass es manchmal sehr gut sein kann, einfach mal gar nichts zu tun und den Dingen ihren Lauf zu lassen. Nicht jede heftige Diskussion ist ein Streit, über den man danach sprechen muss.
Nicht jedes glänzende neue Methoden-Dingsbums steigert die Teamperformance. Ein Bündel schlecht geschriebener Stories erfordert nicht die intensive Schulung oder den Austausch des Product Owners. Ein Teammitglied, dass immer wieder publikumswirksam dasselbe wiederholt spiegelt nicht zwingend die Teammeinung wieder.
An manchen Tagen hat jemand im Team einfach einen miesen Tag und muss sich mal Luft machen - er wird trotzdem nicht morgen kündigen.

Viele dieser Dinge erledigen sich meistens von alleine, ohne das man seine _agile Magie_ wirken muss. Manche dieser Dinge sind ein größeres Problem. Ich muss nur lernen, das eine vom anderen zu unterscheiden...
Also durchatmen, einen Schritt zurück machen, einen Schluck Tee nehmen und einfach auch mal nicht eingreifen!

