---
layout: page
title: about me
permalink: /about/
---

As you are viewing this page right now, you probably want to know who made up this site.

I was born in 1978 in Germany. Currently, I am living in Dorsten (Cologne area as LinkedIn would put it), a small town about 100km north-east of Cologne.

After finishing school and completeing my mandatory service in the German army, I started my job training at a small company in Bergisch Gladbach (which actually is really close to Cologne) and finished with the handy job title of _Fachinformtiker für Anwendungsentwicklung_ - software developer, that is.

After this small company was sold to another only marginally bigger company, I worked as a network administrator for about two years before deciding to go for a degree at university.

In January of 2008 I completed my studies at the [University of Applied Sciences at Dortmund](http://www.inf.fh-dortmund.de) and am eligible to carry the title '_Bachelor of Science in Health Informatics_'. I gained some experience working in the IT department of a statutory health insurance as a Java Developer and Project Manager and as an IT consultant in a consulting agency. Currently I am working as a <s>Project Manager for software projects</s> <s>ScrumMaster</s> Software Developer in a larger company focused on services built around performing billing tasks for energy consumption in properties (among many other things related to managing properties).

I was infected by Linux after first getting in touch with it in 1998, also I got deeper interest in mobile operating systems (especially the now-dead Palm webOS and SailfishOS by [Jolla](http://www.jolla.com)) and web technologies.
Other things in my daily life are typical things that are a bit nerdy - Dilbert, Starwars, Star Trek, Doctor Who, unhealthy food & Coke...

In the real world, I enjoy doing things that are not related to IT at all. Most of these activities involve spending time with my 4 kids (and sometimes with my wife). I play Tennis as a hobby and travel around the country watching Tennis matches of my kids who play far better at young age than I will ever be capable of.

## why 'h-dawg.de' ?

Sounds like a dumb domain name - yeah, you're probably right.

How did it happen that I have chosen this name? Someone gave that nickname to me while I was on a student exchange in  the United States (I was a really cool hip-hopping b-boy at that time). Afterwards, I used _h-dawg_ or _h.dawg_ as a nickname for video games, mail accounts and online services. Of course I imediately secured the domain names, added some mail addresses and never got rid of all of that...

This site used to host the pages of my basketball team followed by the website of the complete basketball club. For a short period, I even hosted a pure static personal homepage. Since 2005 this was the place I put my blog live. It was powered by Wordpress and had about 300 posts - all in German. When I realized I do not need the power of Wordpress to cover my needs for a personal (low traffic) site, I started migrating my blog to [ghost](http://ghost.org). While this was in progress, I decided to start over and create a wild mixture of German and English  posts while trying to cover different topics. Since I spend more time playing aroung with technology than creating content, I next moved to a static site generator again - the current version of this website is created using [Jekyll](https://jekyllrb.com/)

thanks for reading this far, enjoy my site!
